package com.example.customer.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.customer.Model.Invoice;
import com.example.customer.Service.InvoiceService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class InvoiceController {
    @Autowired
    private InvoiceService service;

    @GetMapping("/invoices")
    public List<Invoice> getInvoices() {
        return service.createInvoice();
    }
}
