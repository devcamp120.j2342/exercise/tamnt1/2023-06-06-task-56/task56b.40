package com.example.customer.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.customer.Model.Customer;
import com.example.customer.Model.Invoice;

@Service
public class InvoiceService {

    public List<Invoice> createInvoice() {
        ArrayList<Customer> customerList = new ArrayList<>();
        Customer customer1 = new Customer(1, "Tam", 10);
        Customer customer2 = new Customer(2, "adam", 20);
        Customer customer3 = new Customer(3, "Khan", 20);

        customerList.addAll(Arrays.asList(customer1, customer2, customer3));

        for (Customer customer : customerList) {
            System.out.println(customer);
        }

        ArrayList<Invoice> invoiceList = new ArrayList<>();
        Invoice invoice1 = new Invoice(1, customer1, 10000);
        Invoice invoice2 = new Invoice(1, customer2, 20000);
        Invoice invoice3 = new Invoice(1, customer3, 30000);
        invoiceList.addAll(Arrays.asList(invoice1, invoice2, invoice3));

        for (Invoice invoice : invoiceList) {
            System.out.println(invoice);
        }

        return invoiceList;

    }
}
